const axios = require('axios');
const chai = require('chai');
const assert = chai.assert;
const url_to_analyse = "https://getemoji.com";

describe('Suite Tests', function () {
    this.timeout(0);

    it('Default Endpoint-- True properties', function (done) {
        var obj_keys = ["title", "url", "response_code", "speed_score"];

        axios.get("http://localhost/")
            .then(response => {
                var _res = true

                response.data.map(item => {
                    //Verify that nb properties match with the nb required. for each item
                    assert.equal(Object.keys(item).length, obj_keys.length)
                    obj_keys.forEach(key => {
                        //Verify if properties exist
                        _res = _res && (item.hasOwnProperty(key));
                    })
                })

                assert.isTrue(_res)
                done()

            }).catch(err => { // properties are incorrects.
                done(new Error("Response doesn't respect the required properties"));
            })
    });


    it('Specific Site - True properties', function (done) {
        var obj_keys = ["title", "url", "response_code", "speed_score"];

        axios.get("http://localhost/" + url_to_analyse)
            .then(response => {
                var _res = true
                //Verify that nb properties match with the nb required.
                assert.equal(Object.keys(response.data).length, obj_keys.length)
                obj_keys.forEach(key => {
                    //Verify if properties match requirement
                    assert.isTrue(response.data.hasOwnProperty(key));
                })
                done()

            }).catch(err => { // proporties are incorrects.
                done(new Error("Response doesn't respect the required properties"));
            })
    });



    it('Specific Site - Fake properties (1 changed)', function (done) {
        var obj_keys = ["title", "url", "response_code", "speed_scored"];

        axios.get("http://localhost/" + url_to_analyse)
            .then(response => {
                var _res = true
                //Verify that nb properties match with the nb required.
                assert.equal(Object.keys(response.data).length, obj_keys.length)
                obj_keys.forEach(key => {
                    //Verify if properties match requirements
                    _res = _res && response.data.hasOwnProperty(key)
                })
                assert.isFalse(_res);
                done()

            }).catch(err => { // properties are incorrects.
                done(new Error("Response doesn't respect the required properties"));
            })
    });


});