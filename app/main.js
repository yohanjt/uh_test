const axios = require('axios');
const express = require('express')
const app = express()

const external_api_endpoint = "https://www.googleapis.com/pagespeedonline/v4/runPagespeed";
const external_api_key = "AIzaSyCyNk513GJXD5WXNZxaaKDRk3zaDVhz_48";

const list_sites = [
    'https://www.hotel-internet-marketing.com/',
    'https://www.google.co.uk/',
    'https://www.bbc.co.uk/'
]

app.get('/', function (req, res) {

    // go throught all defualt website
    const promises = list_sites.map(async item => {
       
        const json = await axios({
            method: 'GET',
            url: ext_req_url = external_api_endpoint + "?url=" + item,
        })
        //format the response
        return {
            title: json.data.title,
            response_code: json.data.responseCode,
            speed_score: json.data.ruleGroups.SPEED.score,
            url: json.data.id
        }
    })

    // wait until all promises resolve
    const results = Promise.all(promises)
    results.then(data => res.send(data))
});


app.get('/:prot*', function (req, res) {
    let url_to_analyse = req.params["prot"] + req.params[0];
    let ext_req_url = external_api_endpoint + "?url=" + url_to_analyse;

    axios.get(ext_req_url)
            
        .then(json => ({
            title: json.data.title,
            response_code: json.data.responseCode,
            speed_score: json.data.ruleGroups.SPEED.score,
            url: json.data.id
        }))
        .then(data => res.send(data))
        .catch(function (error) {
            // handle url parameters errors
            
            res.send({
                url:url_to_analyse,
                response_code:error.response.data.error.code,
                message:error.response.data.error.message
            });
            
        });
})

app.listen(80, function () {
    console.log('Example app listening on port 80!')
})